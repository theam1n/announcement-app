package com.example.AnnouncementApp.controller;

import com.example.AnnouncementApp.dto.AnnouncementDto;
import com.example.AnnouncementApp.dto.SearchCriteria;
import com.example.AnnouncementApp.service.impl.AnnouncementServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/announcement")
public class AnnouncementController {

    private final AnnouncementServiceImpl announcementService;

    @PostMapping
    public ResponseEntity<Page<AnnouncementDto>> getAllAnnouncements(
            @RequestBody List<SearchCriteria> searchCriteriaList,
            @RequestParam(value = "pageSize") int pageSize,
            @RequestParam(value = "pageNumber") int pageNumber,
            @RequestParam(value = "pageSort") String[] pageSort) {

        return ResponseEntity.ok(announcementService.
                getAllAnnouncements(searchCriteriaList,pageSize,pageNumber,pageSort));
    }

    @GetMapping("/mostViewed")
    public ResponseEntity<Page<AnnouncementDto>> getMostViewedAnnouncements(
            @RequestParam(value = "pageSize") int pageSize,
            @RequestParam(value = "pageNumber") int pageNumber,
            @RequestParam(value = "pageSort") String[] pageSort) {

        return ResponseEntity.ok(announcementService.
                getMostViewedAnnouncements(pageSize,pageNumber,pageSort));
    }

}
