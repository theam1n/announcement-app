package com.example.AnnouncementApp.controller;

import com.example.AnnouncementApp.dto.AnnouncementDto;
import com.example.AnnouncementApp.dto.AnnouncementRequest;
import com.example.AnnouncementApp.service.impl.AnnouncementServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/user")
public class UserAnnouncementController {

    private final AnnouncementServiceImpl announcementService;

    @PostMapping("/announcement")
    public ResponseEntity<AnnouncementDto> createAnnouncement(
            @RequestParam(value = "username") String username,
            @RequestParam(value = "password") String password,
            @RequestBody AnnouncementRequest announcementRequest
    ){

        AnnouncementDto response = announcementService.createAnnouncement(
                username, announcementRequest);
        return new ResponseEntity<>(response, HttpStatus.CREATED);

    }

    @PutMapping("/announcement/{id}")
    public ResponseEntity<AnnouncementDto> updateAnnouncement(
            @PathVariable Long id,
            @RequestParam(value = "username") String username,
            @RequestParam(value = "password") String password,
            @RequestBody AnnouncementRequest announcementRequest
    ){
        AnnouncementDto response = announcementService.updateAnnouncement(
                username,id, announcementRequest);
        return ResponseEntity.ok(response);
    }

    @DeleteMapping("/announcement/{id}")
    public void deleteAnnouncement(
            @PathVariable Long id,
            @RequestParam(value = "username") String username,
            @RequestParam(value = "password") String password
    ){
        announcementService.deleteAnnouncement(username, id);
    }

    @GetMapping("/announcement/{id}")
    public ResponseEntity<AnnouncementDto> getOwnAnnouncementWithId(
            @PathVariable Long id,
            @RequestParam(value = "username") String username,
            @RequestParam(value = "password") String password
    ){
        AnnouncementDto response = announcementService.getOwnAnnouncementWithId(
                username, id);
        return ResponseEntity.ok(response);
    }

    @GetMapping("/announcement/mostViewed")
    public ResponseEntity<AnnouncementDto> getOwnMostViewedAnnouncement(
            @RequestParam(value = "username") String username,
            @RequestParam(value = "password") String password
    ){
        AnnouncementDto response = announcementService.getOwnMostViewedAnnouncement(username);
        return ResponseEntity.ok(response);
    }

    @GetMapping
    @Cacheable(cacheNames = "announcement",key = "#username")
    public Page<AnnouncementDto> getAllOwnAnnouncement(
            @RequestParam(value = "username") String username,
            @RequestParam(value = "password") String password,
            @RequestParam(value = "pageSize") int pageSize,
            @RequestParam(value = "pageNumber") int pageNumber,
            @RequestParam(value = "pageSort") String[] pageSort) {

        return announcementService.getAllOwnAnnouncement(username,pageSize,pageNumber,pageSort);
    }
}
