package com.example.AnnouncementApp.controller;

import com.example.AnnouncementApp.dto.UserDto;
import com.example.AnnouncementApp.dto.UserRequest;
import com.example.AnnouncementApp.service.impl.UserServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/auth")
public class AuthController {

    private final UserServiceImpl userService;

    @PostMapping("/register")
    ResponseEntity<UserDto> register (@RequestBody UserRequest userRequest){
        UserDto response = userService.register(userRequest);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

}
