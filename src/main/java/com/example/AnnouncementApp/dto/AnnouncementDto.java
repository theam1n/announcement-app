package com.example.AnnouncementApp.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AnnouncementDto implements Serializable {

    private Long id; // Announcement's id

    private int viewCount;

    private String title;

    private String description;

    private double price;

    private String name; // user's name

    private String surname; // user's surname
}
