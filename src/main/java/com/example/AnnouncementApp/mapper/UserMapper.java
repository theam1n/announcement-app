package com.example.AnnouncementApp.mapper;

import com.example.AnnouncementApp.dto.UserDto;
import com.example.AnnouncementApp.dto.UserRequest;
import com.example.AnnouncementApp.entity.User;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring",
        nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS,
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface UserMapper {

    public static final UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    @Mapping(target = "id", ignore = true)
    User requestToEntity(UserRequest userRequest);

    UserDto entityToDto(User user);
}
