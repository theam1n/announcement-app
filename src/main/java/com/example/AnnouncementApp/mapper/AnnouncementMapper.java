package com.example.AnnouncementApp.mapper;


import com.example.AnnouncementApp.dto.AnnouncementDto;
import com.example.AnnouncementApp.dto.AnnouncementRequest;
import com.example.AnnouncementApp.entity.Announcement;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring",
        nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS,
        nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface AnnouncementMapper {

    public static final AnnouncementMapper INSTANCE = Mappers.getMapper(AnnouncementMapper.class);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "announcementDetail.title", source = "title")
    @Mapping(target = "announcementDetail.description", source = "description")
    @Mapping(target = "announcementDetail.price", source = "price")
    Announcement requestToEntity(AnnouncementRequest announcementRequest);

    @Mapping(target = "title", source = "announcementDetail.title")
    @Mapping(target = "description", source = "announcementDetail.description")
    @Mapping(target = "price", source = "announcementDetail.price")
    @Mapping(target = "name", source = "user.name")
    @Mapping(target = "surname", source = "user.surname")
    AnnouncementDto entityToDto(Announcement announcement);
}
