package com.example.AnnouncementApp.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.time.LocalDateTime;

@Entity
@Table(name = "announcement_details")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class AnnouncementDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String title;

    private String description;

    private double price;

    @CreationTimestamp
    private LocalDateTime createDate;

    @UpdateTimestamp
    private LocalDateTime updateDate;

    @OneToOne
    @JoinColumn(name = "announcement_id")
    @ToString.Exclude
    @JsonIgnore
    private Announcement announcement;


}
