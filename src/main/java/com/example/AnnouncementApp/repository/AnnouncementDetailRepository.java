package com.example.AnnouncementApp.repository;

import com.example.AnnouncementApp.entity.AnnouncementDetail;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AnnouncementDetailRepository extends JpaRepository<AnnouncementDetail, Long> {
}
