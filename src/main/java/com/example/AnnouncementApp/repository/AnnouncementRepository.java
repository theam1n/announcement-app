package com.example.AnnouncementApp.repository;

import com.example.AnnouncementApp.entity.Announcement;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import java.util.List;
import java.util.Optional;

public interface AnnouncementRepository extends JpaRepository<Announcement, Long>, JpaSpecificationExecutor<Announcement> {

    Page<Announcement> findAllByOrderByViewCountDesc(Pageable pageable);

    Page<Announcement> findAllByUser_Username(String username,Pageable pageable);

    Optional<Announcement> findByIdAndUser_Username(Long id, String username);

    Optional<Announcement> findTopByUser_UsernameOrderByViewCountDesc(String username);


}
