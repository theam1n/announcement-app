package com.example.AnnouncementApp.repository;

import com.example.AnnouncementApp.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    boolean existsByUsernameAndPassword(String username,String password);

    Optional<User> findByUsername(String username);
}
