package com.example.AnnouncementApp;

import com.example.AnnouncementApp.dto.AnnouncementDto;
import com.example.AnnouncementApp.dto.AnnouncementRequest;
import com.example.AnnouncementApp.entity.Announcement;
import com.example.AnnouncementApp.entity.AnnouncementDetail;
import com.example.AnnouncementApp.entity.User;
import com.example.AnnouncementApp.mapper.AnnouncementMapper;
import com.example.AnnouncementApp.repository.AnnouncementDetailRepository;
import com.example.AnnouncementApp.repository.AnnouncementRepository;
import com.example.AnnouncementApp.service.impl.AnnouncementServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class AnnouncementAppApplication{

	public static void main(String[] args) {
		SpringApplication.run(AnnouncementAppApplication.class, args);

	}

}
