package com.example.AnnouncementApp.filter;

import com.example.AnnouncementApp.dto.LoginRequest;
import com.example.AnnouncementApp.service.impl.UserServiceImpl;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;

@Component
@RequiredArgsConstructor
public class AuthFilter extends OncePerRequestFilter {

    private final UserServiceImpl userService;

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {

        String requestURI = request.getRequestURI();
        LoginRequest loginRequest = new LoginRequest(request.getParameter("username")
                ,request.getParameter("password"));


        if (requestURI.startsWith("/announcement-app/api/v1/user")){

            if (loginRequest.getUsername() == null || loginRequest.getPassword() == null){
                response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Username or Password must not be null");
                return;
            }

            if (!userService.login(loginRequest)) {
                response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized access");
                return;
            }
        }

        filterChain.doFilter(request,response);

    }
}
