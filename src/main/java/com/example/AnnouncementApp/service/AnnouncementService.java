package com.example.AnnouncementApp.service;

import com.example.AnnouncementApp.dto.AnnouncementDto;
import com.example.AnnouncementApp.dto.AnnouncementRequest;
import com.example.AnnouncementApp.dto.SearchCriteria;
import org.springframework.data.domain.Page;

import java.util.List;

public interface AnnouncementService {

    AnnouncementDto createAnnouncement(String username, AnnouncementRequest announcementRequest);

    AnnouncementDto updateAnnouncement(String username,Long id, AnnouncementRequest announcementRequest);

    void deleteAnnouncement(String username, Long id);

    AnnouncementDto getOwnAnnouncementWithId(String username,Long id);

    AnnouncementDto getOwnMostViewedAnnouncement(String username);

    Page<AnnouncementDto> getAllOwnAnnouncement(String username,int pageSize, int pageNumber, String[] pageSort);

    Page<AnnouncementDto> getMostViewedAnnouncements(int pageSize, int pageNumber, String[] pageSort);

    Page<AnnouncementDto> getAllAnnouncements(List<SearchCriteria> searchCriteriaList, int pageSize, int pageNumber, String[] pageSort);


}


