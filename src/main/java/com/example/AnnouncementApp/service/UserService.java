package com.example.AnnouncementApp.service;

import com.example.AnnouncementApp.dto.LoginRequest;
import com.example.AnnouncementApp.dto.UserDto;
import com.example.AnnouncementApp.dto.UserRequest;

public interface UserService {

    UserDto register(UserRequest userRequest);

    boolean login(LoginRequest loginRequest);
}
