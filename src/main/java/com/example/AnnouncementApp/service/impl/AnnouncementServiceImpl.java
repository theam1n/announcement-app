package com.example.AnnouncementApp.service.impl;

import com.example.AnnouncementApp.dto.AnnouncementDto;
import com.example.AnnouncementApp.dto.AnnouncementRequest;
import com.example.AnnouncementApp.dto.SearchCriteria;
import com.example.AnnouncementApp.entity.AnnouncementDetail;
import com.example.AnnouncementApp.mapper.AnnouncementMapper;
import com.example.AnnouncementApp.repository.AnnouncementDetailRepository;
import com.example.AnnouncementApp.repository.AnnouncementRepository;
import com.example.AnnouncementApp.repository.UserRepository;
import com.example.AnnouncementApp.service.AnnouncementService;
import com.example.AnnouncementApp.specification.AnnouncementSpecification;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class AnnouncementServiceImpl implements AnnouncementService {

    private final AnnouncementRepository announcementRepository;
    private final UserRepository userRepository;

    private final AnnouncementDetailRepository announcementDetailRepository;

    @Override
    public AnnouncementDto createAnnouncement(String username,AnnouncementRequest announcementRequest) {
        var announcement = AnnouncementMapper.INSTANCE.requestToEntity(announcementRequest);
        var user = userRepository.findByUsername(username).orElseThrow();
        announcement.setUser(user);
        var response = announcementRepository.save(announcement);
        response.getAnnouncementDetail().setAnnouncement(response);
        announcementDetailRepository.save(response.getAnnouncementDetail());
        return AnnouncementMapper.INSTANCE.entityToDto(response);
    }

    @Override
    public AnnouncementDto updateAnnouncement(String username,Long id,AnnouncementRequest announcementRequest) {

        var existingAnnouncement = announcementRepository.findByIdAndUser_Username(
                id, username).orElseThrow();

        Optional.ofNullable(announcementRequest.getPrice()).ifPresent(
                existingAnnouncement.getAnnouncementDetail()::setPrice);
        Optional.ofNullable(announcementRequest.getTitle()).ifPresent(
                existingAnnouncement.getAnnouncementDetail()::setTitle);
        Optional.ofNullable(announcementRequest.getDescription()).ifPresent(
                existingAnnouncement.getAnnouncementDetail()::setDescription);

        var response = AnnouncementMapper.INSTANCE.entityToDto(
                announcementRepository.save(existingAnnouncement));

        return response;
    }

    @Override
    public void deleteAnnouncement(String username,Long id) {
          var announcement = announcementRepository.findByIdAndUser_Username(
                  id, username).orElseThrow();
          announcementRepository.deleteById(id);
    }

    @Override
    public AnnouncementDto getOwnAnnouncementWithId(String username,Long id) {
        var announcement = announcementRepository.findByIdAndUser_Username(
                id, username).orElseThrow();
        var response = AnnouncementMapper.INSTANCE.entityToDto(announcement);
        return response;
    }

    @Override
    public AnnouncementDto getOwnMostViewedAnnouncement(String username) {
        var announcement = announcementRepository.
                findTopByUser_UsernameOrderByViewCountDesc(username).orElseThrow();
        var response = AnnouncementMapper.INSTANCE.entityToDto(announcement);
        return response;
    }

    @Override
    public Page<AnnouncementDto> getAllOwnAnnouncement(String username,int pageSize, int pageNumber, String[] pageSort) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize, Sort.by(pageSort[0]).ascending());
        var announcements = announcementRepository.
                findAllByUser_Username(username,pageable);

        return announcements.map(AnnouncementMapper.INSTANCE::entityToDto);
    }

    @Override
    public Page<AnnouncementDto> getMostViewedAnnouncements(int pageSize, int pageNumber, String[] pageSort) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize, Sort.by(pageSort[0]).ascending());
        var announcements = announcementRepository.
                findAllByOrderByViewCountDesc(pageable);

        return announcements.map(AnnouncementMapper.INSTANCE::entityToDto);
    }

    @Override
    public Page<AnnouncementDto> getAllAnnouncements(
            List<SearchCriteria> searchCriteriaList, int pageSize, int pageNumber, String[] pageSort) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize, Sort.by(pageSort[0]).ascending());

        AnnouncementSpecification announcementSpecification = new AnnouncementSpecification();
        searchCriteriaList.forEach((searchCriteria)-> announcementSpecification.add(searchCriteria));
        var announcements = announcementRepository.
                findAll(announcementSpecification,pageable);
        return announcements.map(AnnouncementMapper.INSTANCE::entityToDto);
    }


}
