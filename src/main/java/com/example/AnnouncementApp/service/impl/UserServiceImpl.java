package com.example.AnnouncementApp.service.impl;

import com.example.AnnouncementApp.dto.LoginRequest;
import com.example.AnnouncementApp.dto.UserDto;
import com.example.AnnouncementApp.dto.UserRequest;
import com.example.AnnouncementApp.mapper.UserMapper;
import com.example.AnnouncementApp.repository.UserRepository;
import com.example.AnnouncementApp.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Override
    public UserDto register(UserRequest userRequest) {
        var user = UserMapper.INSTANCE.requestToEntity(userRequest);
        var response = UserMapper.INSTANCE.entityToDto(userRepository.save(user));
        return response;
    }

    @Override
    public boolean login(LoginRequest loginRequest) {
        return userRepository.existsByUsernameAndPassword(loginRequest.getUsername(), loginRequest.getPassword());
    }
}
